# @Author: Maxime Cohen <maxime>
# @Date:   2017-Jul-24
# @Email:  maxime.cohen-pro@outlook.fr
# @Project: Snake
# @Filename: Makefile
# @Last modified by:   maxime
# @Last modified time: 2017-Jul-24 16:51

CC		=	g++

NAME		=	snake

SRC		=	src/main.cpp \
			src/SDLLib.cpp

OBJ		=	$(SRC:.cpp=.o)

CPPFLAGS	=	-I./include/
CPPFLAGS	+=	-W -Wall -Wextra -pedantic

LDFLAGS		=	-lSDLmain -lSDL -lSDL_image -lSDL_ttf -lSDL_mixer

RM		=	rm -fr

all:	$(NAME)

$(NAME):	$(OBJ)
	$(CC) -o $(NAME) $(CPPFLAGS) $(LDFLAGS) $(OBJ)

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re:		fclean all

.PHONY:	all clean fclean re
