/**
 * @Author: Maxime Cohen <maxime>
 * @Date:   2017-Jul-29 17:41
 * @Email:  maxime.cohen-pro@outlook.fr
 * @Project: Snake
 * @Filename: Game.hh
 * @Last modified by:   maxime
 * @Last modified time: 2017-Jul-29 18:03
 */

#ifndef GAME_HH_
# define GAME_HH_

# include "SDLLib.hh"

class   Game
{
private:
    SDLLib  *sdl;

public:
    Game();
    ~Game();
};

#endif /* !GAME_HH_ */
