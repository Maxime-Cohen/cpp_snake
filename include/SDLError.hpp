/**
 * @Author: Maxime Cohen <maxime>
 * @Date:   2017-Jul-13
 * @Email:  maxime.cohen-pro@outlook.fr
 * @Project: Snake
 * @Filename: SDLError.hpp
 * @Last modified by:   maxime
 * @Last modified time: 2017-Jul-24
 */

#ifndef SDLERROR_HPP_
# define SDLERROR_HPP_

# include <exception>
# include <string>

class		SDLError : public std::exception
{
private:
  std::string	_error;
  char		*_SDL_error;

public:
  SDLError(std::string const &error="", char * const SDL_error=NULL): _error(error), _SDL_error(SDL_error)
  {
  }

  virtual const char *what() const throw()
  {
    return (this->_error + std::string(this->_SDL_error)).c_str();
  }

  virtual ~SDLError() throw()
  {
  }
};

#endif /* !SDLERROR_HPP_ */
