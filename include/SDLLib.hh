/**
 * @Author: Maxime Cohen <maxime>
 * @Date:   2017-Jul-23
 * @Email:  maxime.cohen-pro@outlook.fr
 * @Project: Snake
 * @Filename: SDLLib.hh
 * @Last modified by:   maxime
 * @Last modified time: 2017-Aug-03 21:09
 */

#ifndef SDLLIB_HH_
# define SDLLIB_HH_

# include <iostream>
# include <string>
# include <map>
# include <vector>
# include <utility>
# include "SDL/SDL.h"
# include "SDLError.hpp"

class		SDLLib
{
private:
  	SDL_Surface								*_screen;
  	std::map<unsigned int, SDL_Surface*>	_surfaces;
  	std::vector<SDL_Event *>				_events;
  	Uint8									*_keystates;
	bool									_quit;

public:
  	SDLLib(unsigned int flags=SDL_INIT_EVERYTHING);
  	~SDLLib();

private:
  	void		_Init(unsigned int);
  	SDL_Surface	*_SetVideoMode(int, int, int, unsigned int);
  	SDL_Surface	*_LoadBMP(const std::string &);
  	SDL_Surface	*_DisplayFormat(SDL_Surface *);
  	void		_BlitSurface(SDL_Surface *, SDL_Rect *,
      SDL_Surface *, SDL_Rect *);
  	void		_WM_SetCaption(const std::string &,
      const std::string &);
  	void		_Flip(SDL_Surface *);

public:
  	void	    initScreen(int, int, int, unsigned int);
  	void	    setWindow(const std::string &, const std::string &);
  	void	    addSurface(const std::string &, unsigned int);
  	void		moveSurface(unsigned int, int, int);
  	void		update();
  	void		wait(unsigned int);
	bool		isExit() const;
	bool		isKeyPressed(Uint8) const;
};

#endif /* !SDLLIB_HH_ */
