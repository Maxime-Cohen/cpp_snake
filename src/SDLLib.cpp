/**
 * @Author: Maxime Cohen <maxime>
 * @Date:   2017-Jul-24
 * @Email:  maxime.cohen-pro@outlook.fr
 * @Project: Snake
 * @Filename: SDLLib.cpp
 * @Last modified by:   maxime
 * @Last modified time: 2017-Aug-07 20:27
 */

#include "SDLLib.hh"

// Constructors, Destructors

SDLLib::SDLLib(unsigned int flags)
{
  this->_Init(flags);
  this->_screen = NULL;
  this->_keystates = NULL;
  this->_quit = false;
}

SDLLib::~SDLLib()
{
  std::map<unsigned int, SDL_Surface*>::iterator	itr;

  if (this->_screen)
    SDL_FreeSurface(this->_screen);
  for(itr = this->_surfaces.begin(); itr != this->_surfaces.end(); ++itr)
    SDL_FreeSurface((*itr).second);
  SDL_Quit();
}


// Private methods

void	SDLLib::_Init(unsigned int flags)
{
  if (SDL_Init(flags) != 0)
    throw SDLError("Can't init SDL: ", SDL_GetError());
}

SDL_Surface	*SDLLib::_SetVideoMode(int width, int height,
				       int bpp, unsigned int flags)
{
  SDL_Surface	*surface = NULL;

  surface = SDL_SetVideoMode(width, height, bpp, flags);
  if (surface == NULL)
    throw SDLError("Can't set SDL Video Mode: ", SDL_GetError());
  return (surface);
}

SDL_Surface	*SDLLib::_LoadBMP(const std::string &filename)
{
  SDL_Surface	*surface = NULL;

  surface = SDL_LoadBMP(filename.c_str());
  if (surface == NULL)
    throw SDLError("Can't load BMP: ", SDL_GetError());
  return (surface);
}

SDL_Surface	*SDLLib::_DisplayFormat(SDL_Surface *surface)
{
  SDL_Surface	*newSurface = NULL;

  newSurface = SDL_DisplayFormat(surface);
  if (newSurface == NULL)
    throw SDLError("Can't convert the surface to the display format: ", SDL_GetError());
  return (newSurface);
}

void		SDLLib::_BlitSurface(SDL_Surface *src,
				     SDL_Rect *srcrect,
				     SDL_Surface *dest,
				     SDL_Rect *destrect)
{
  if (SDL_BlitSurface(src, srcrect, dest, destrect) != 0)
    throw SDLError("Can't Blit the surface: ", SDL_GetError());
}

void		SDLLib::_WM_SetCaption(const std::string &title,
				       const std::string &icon)
{
  SDL_WM_SetCaption(title.c_str(), icon.c_str());
}

void		SDLLib::_Flip(SDL_Surface *screen)
{
  if (SDL_Flip(screen) != 0)
    throw SDLError("Can't Flip on the screen: ", SDL_GetError());
}

// Public methods

void		SDLLib::initScreen(int width, int height,
				   int bpp, unsigned int flags)
{
  this->_screen = this->_SetVideoMode(width, height, bpp, flags);
}

void		SDLLib::setWindow(const std::string &title,
				  const std::string &icon)
{
  this->_WM_SetCaption(title, icon);
}

void		SDLLib::addSurface(const std::string &filename,
				   unsigned int id)
{
  SDL_Surface	*loadSurface;
  SDL_Surface	*optiSurface;

  try
    {
      loadSurface = this->_LoadBMP(filename);
      optiSurface = this->_DisplayFormat(loadSurface);
    }
  catch(const SDLError &e)
    {
      throw;
    }
  SDL_FreeSurface(loadSurface);
  this->_surfaces[id] = optiSurface;
}

void			SDLLib::moveSurface(unsigned int id, int x, int y)
{
    SDL_Rect    offset;

    offset.x = x;
    offset.y = y;
    this->_BlitSurface(this->_screen, NULL,
        this->_surfaces[id], &offset);
}

void			SDLLib::update()
{
    this->_Flip(this->_screen);
}

void 			SDLLib::wait(unsigned int milisec)
{
    SDL_Delay(milisec);
}

bool			SDLLib::isExit() const
{
	return (this->_quit);
}

bool			SDLLib::isKeyPressed(Uint8 key) const
{
	return ((bool)this->_keystates[key]);
}
