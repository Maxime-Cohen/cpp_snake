/**
 * @Author: Maxime Cohen <maxime>
 * @Date:   2017-Jul-21 04:03
 * @Email:  maxime.cohen-pro@outlook.fr
 * @Project: Snake
 * @Filename: main.cpp
 * @Last modified by:   maxime
 * @Last modified time: 2017-Jul-29 17:34
 */

#include <iostream>
#include <exception>
#include <stdlib.h>
#include <stdio.h>
#include "SDLLib.hh"

int	main(int ac, char **av)
{
  SDLLib	*sdl;

  (void)ac;
  (void)av;
  try
    {
      sdl = new SDLLib(SDL_INIT_EVERYTHING);
      sdl->initScreen(640, 480, 16, SDL_HWSURFACE);
      sdl->wait(6000);
    }
  catch (std::exception const &e)
    {
      std::cerr << "ERROR: " << e.what() << std::endl;
    }
  delete sdl;
  return EXIT_SUCCESS;
}
